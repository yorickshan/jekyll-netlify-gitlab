# Deploying a Jekyll Site to Netlify with Docker and GitLab CI

[![pipeline status](https://gitlab.com/michaelherman/jekyll-netlify-gitlab/badges/master/pipeline.svg)](https://gitlab.com/michaelherman/jekyll-netlify-gitlab/commits/master)

## Want to learn how to build this?

Check out the [post](https://mherman.org/blog/deploying-jekyll-netlify-docker-gitlab-ci/).

## Want to use this project?

### Run locally

1. Install a full Ruby development environment. You need a version of Ruby > 2.4 along with Gem. See [https://jekyllrb.com/docs/installation/](https://jekyllrb.com/docs/installation/) for help.


1. Install Jekyll and bundler gems:

    ```sh
    $ gem install jekyll bundler
    ```

1. Install dependencies:

    ```sh
    $ bundle install --gemfile=./src/Gemfile
    ```

1. Create a build and run it locally:

    ```sh
    $ cd src
    $ bundle exec jekyll serve
    ```

    Navigate to [http://localhost:4000](http://localhost:4000)

### Development Workflow

1. Create a new feature branch
1. Make changes
1. Commit and push your code to GitLab
1. GitLab CI will trigger a new build
1. On success, navigate to the Netlify admin to get the preview URL
1. If all is well, create a PR to merge the feature branch into master
1. After merge, GitLab CI will trigger a new build
1. On success, publish the deploy in the Netlify admin
